var produtoModule = angular.module('produtoModule',[]);

produtoModule.controller("produtoControl",function($scope,$http) {
		
	
	urlProduto = 'http://localhost:8080/scgp/';	

	$scope.pesquisarProduto = function(){		
		$http.get(urlProduto+"listar").success(function (produtos){						
			$scope.produtos = produtos;
		}).error(function (erro){
			console.log(erro);
		});
	}

	$scope.salvar = function(){
		if($scope.produto.codigo = ''){			
			var x = angular.toJson($scope.produto)
			console.log( x  );
			$http.post(urlProduto+"save", x).success(function(produto){
				$scope.produtos.push($scope.produto);
				$scope.novo();
			}).error(function (erro){
				alert(erro);
			});
		}else{
			var x = angular.toJson($scope.produto)
			console.log( x  );
			$http.post(urlProduto+"save",x).success(function(produto){
				$scope.pesquisarProduto();
				$scope.novo();
			}).error(function (erro){
				alert(erro);
			});
		}		
	}

	$scope.excluir = function(){
		if($scope.produto.codigo == ''){
			alert("Selecione um produto");
		}else{
			urlExcluir = urlProduto + "delete/" + $scope.produto.codigo;
			$http.get(urlExcluir).success(function (){
				$scope.pesquisarProduto();
				$scope.novo();
			}).error(function (erro){
				alert(erro);
			})
		}		
	}

	
	$scope.novo = function() {
		$scope.produto = "";
	}
	
	$scope.selecionaProduto = function(produto) {
		$scope.produto = produto;
	}		
	$scope.pesquisarProduto();

	function params(obj){
	    var str = "";
	    for (var key in obj) {
	        if (str != "") {
	            str += "&";
	        }
	        str += key + "=" + encodeURIComponent(obj[key]);
	    }
	    return str;
	}
	
});	